select x.name as employee_name,json_agg(json_build_object('date',t1,'workload',t2)ORDER BY (t1))
from (
         select  empl.employee_name as name,mw.start_date as t1,sum(mw.workload) as t2 from m_employee empl
         join m_position pos on empl.employee_id = pos.assignee_id
         join m_workload mw on pos.position_id = mw.position_id
        where pos.status = 'Assigned'
         group by employee_id,mw.start_date order by employee_id
     ) x
group by x.name