select position_id,project_code,r.role_name,pos.status as position_status, proj.status as project_status, bill.billing_type_name, 
	concat_ws('/', c1.location_name, c.location_name) as position_country_city,emp.employee_name as assignee_name from m_position pos 
	join m_project proj on
	pos.project_id = proj.project_id
	join c_role r on
	pos.role_id = r.role_id
	join c_billing_type bill on
	pos.billing_type_id = bill.billing_type_id
	left join m_employee emp on
	pos.assignee_id = emp.employee_id
	join c_location c on pos.location_id = c.location_id
    join c_location c1 on c.parent_id = c1.location_id
    where pos.status != 'Closed' 
	order by proj.project_code
	