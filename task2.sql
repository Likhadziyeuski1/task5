with recursive subord as (
    select m.unit_id, m.unit_name, cast(m.unit_name as varchar(100)) as path,m.manager_id,count(empl.employee_id) as value1 
    from m_unit m  join  m_employee empl on m.unit_id = empl.unit_id
    where parent_id is null
	group by m.unit_id
    union all
    select m.unit_id,m.unit_name,cast(concat_ws('/',s.path,m.unit_name) as varchar(100)),m.manager_id,count2 from (select m.unit_id as id,
		count(empl.employee_id) as count2
    from m_unit m  join  m_employee empl on m.unit_id = empl.unit_id group by m.unit_id) 
   	w  join m_unit m on m.unit_id = w.id
      inner join subord s on s.unit_id = m.parent_id
)
select s.unit_id,s.unit_name,s.path,empl.employee_name,s.value1 as direct_count,sum(s1.value1) as total_count
from subord s
	join subord s1  on  s1.path like CONCAT('%',s.path, '%')
	join m_employee empl on s.manager_id = empl.employee_id
group by s.unit_id,s.unit_name,s.path,s.value1,empl.employee_name
order by path

